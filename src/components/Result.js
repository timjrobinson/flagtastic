import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

function Result(props) {

  return (
    <ReactCSSTransitionGroup
      className="container result"
      component="div"
      transitionName="fade"
      transitionEnterTimeout={800}
      transitionLeaveTimeout={500}
      transitionAppear
      transitionAppearTimeout={500}
    >
      <div>
        You scored <strong>{props.score} out of 10!</strong>
      </div>
    </ReactCSSTransitionGroup>
  );

}

Result.propTypes = {
  score: React.PropTypes.number.isRequired,
};

export default Result;
