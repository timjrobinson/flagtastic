import json
import datetime
import string
import random
import boto3
from game.helpers import create_error_response

client = boto3.client('dynamodb')

def handler(event, context):
    if 'body' in event:
        event = json.loads(event['body'])
    if 'code' not in event:
        return create_error_response("code is a required parameter", 400)
    code = event['code']
    
    if 'player_id' not in event:
        return create_error_response("player_id is a required parameter", 400)
    player_id = event['player_id']
        
    game_info = client.get_item(
        TableName='games', 
        Key={'code': {'S': code}}
    )
    if 'Item' not in game_info:
        return {
            'statusCode': 404,
            'body': {
                'error': f"Could not find game of code {code}"
            },
            'headers': {'Content-Type': 'application/json'}
        }
        
    game_state = game_info['Item']['game_state']['N']
    if game_state != '1':
        return {
            'statusCode': 403,
            'body': {
                'error': f"Could not join game - game has already started"
            },
            'headers': {'Content-Type': 'application/json'}
        }
        
    updated_game = client.update_item(
        TableName='games',
        Key={'code': {'S': code}},
        UpdateExpression="ADD player_ids :element",
        ExpressionAttributeValues={
            ':element': {
                'SS': [player_id]
            }
        }
    )
        
    return {
        'statusCode': 200,
        'body': json.dumps({
            'success': 'true'
        }),
        'headers': {'Content-Type': 'application/json'}
    }
