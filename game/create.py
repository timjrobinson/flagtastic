import json
import datetime
import string
import random
import boto3
from game.questions import generate_question
from game.helpers import create_error_response

client = boto3.client('dynamodb')

total_questions = 10

def generate_questions(code):
    for num in range(0, total_questions):
        question = generate_question()
        response = client.put_item(
            TableName='questions',
            Item = {
                'game_code': { 'S': code },
                'question_num': { 'N': str(num) },
                'text': { 'S': question['text'] },
                'answers': { 'L': list(map(lambda a: {'S': a}, question['answers'])) },
                'correct_answer': { 'N': str(question['correct_answer']) }
            }
        )
    
def handler(event, context):
    if 'body' in event:
        event = json.loads(event['body'])
    if 'player_id' not in event:
        return create_error_response(f"player_id is a required parameter {event}", 400)
        
    player_id = event['player_id']
    code = ''.join(random.choices(string.ascii_uppercase, k=6))
    response = client.put_item( 
        TableName = 'games',
        Item = {
            'code': { 'S': code },
            'state': { 'N': '1' },
            'player_ids': { 'SS': [ player_id ] }
        }
    )
    generate_questions(code) 
    
    return {
        'statusCode': 200,
        'body': json.dumps({
            'code': code
        }),
        'headers': {'Content-Type': 'application/json'}
    }
