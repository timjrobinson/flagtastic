import json
import datetime
import string
import random
import boto3
from game.helpers import create_error_response

client = boto3.client('dynamodb')

def handler(event, context):
    if 'body' in event:
        event = json.loads(event['body'])
    if 'code' not in event:
        return create_error_response("code is a required parameter", 400)
    code = event['code']
    
    if 'question_num' not in event:
        return create_error_response("question_num is a required parameter", 400)
    question_num = event['question_num']
        
    question_data = client.get_item(
        TableName='questions', 
        Key={'game_code': {'S': code}, 'question_num': {'N': question_num}}
    )
    if 'Item' not in question_data:
        return {
            'statusCode': 404,
            'body': {
                'error': f"Could not find question {question_num} for game {code}"
            },
            'headers': {'Content-Type': 'application/json'}
        }
        
    question = question_data['Item']
    return {
        'statusCode': 200,
        'body': json.dumps({
            'text': question['text']['S'],
            'answers': list(map(lambda a: a['S'], question['answers']['L'])),
        }),
        'headers': {'Content-Type': 'application/json'}
    }
