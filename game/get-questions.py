import json
import datetime
import string
import random
import boto3
from game.helpers import create_error_response

client = boto3.client('dynamodb')

def handler(event, context):
    if 'body' in event:
        event = json.loads(event['body'])
    if 'code' not in event:
        return create_error_response("code is a required parameter", 400)
    code = event['code']
    
    question_data = client.scan(
        TableName='questions', 
        FilterExpression = 'game_code = :value',
        ExpressionAttributeValues = {
            ":value":  {"S": code}
        }
    )
    if 'Items' not in question_data:
        return {
            'statusCode': 404,
            'body': {
                'error': f"Could not find question for game {code}"
            },
            'headers': {'Content-Type': 'application/json'}
        }
    
    parsedItems = []
    for question in question_data['Items']:
        parsedItems.append({
            'text': question['text']['S'],
            'answers': list(map(lambda a: a['S'], question['answers']['L'])),
            'correct_answer': question['correct_answer']['N']
        })
        
    return {
        'statusCode': 200,
        'body': json.dumps(parsedItems),
        'headers': {'Content-Type': 'application/json'}
    }
