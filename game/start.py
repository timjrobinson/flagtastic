import json
import datetime
import string
import random
import boto3
from game.helpers import create_error_response

client = boto3.client('dynamodb')

def handler(event, context):
    if 'body' in event:
        event = json.loads(event['body'])
    if 'code' not in event:
        return create_error_response("code is a required parameter", 400)
    code = event['code']
    
    game_info = client.get_item(
        TableName='games', 
        Key={'code': {'S': code}}
    )
    if 'Item' not in game_info:
        return {
            'statusCode': 404,
            'body': {
                'error': f"Could not find game of code {code}"
            },
            'headers': {'Content-Type': 'application/json'}
        }
        
    game_state = game_info['Item']['state']['N']
    if game_state != '1':
        return {
            'statusCode': 403,
            'body': {
                'error': f"Could not start game - game has already started"
            },
            'headers': {'Content-Type': 'application/json'}
        }
        
    updated_game = client.update_item(
        TableName='games',
        Key={'code': {'S': code}},
        UpdateExpression='SET game_state = :newstate',
        ExpressionAttributeValues={
            ':newstate': { 'S': '2' }
        }
    )
        
    return {
        'statusCode': 200,
        'body': json.dumps({
            'success': 'true'
        }),
        'headers': {'Content-Type': 'application/json'}
    }
