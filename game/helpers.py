import json

def create_error_response(err, status_code = 400): 
    return {
        'statusCode': status_code,
        'body': json.dumps({
            'error': err
        }),
        'headers': {'Content-Type': 'application/json'}
    }
    
